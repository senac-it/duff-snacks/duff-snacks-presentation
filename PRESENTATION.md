---
marp: true
theme: default
transition: implode
style: |
  :root {
    font-size: 1.4rem;
  }
  table {
    font-size: 1.1rem;
  }
  section.center-all * {
    text-align: center;
    margin-left: auto;
    margin-right: auto;
  }
  section.center-h2 h2 {
    text-align: center;
  }
  section.right {
    text-align: right;
  }
---

# Duff Snacks

## Projeto Integrador III

![bg right 50%](./images/logo.svg)

---

## Introdução

Um Sistema interno de um cardápio eletrônico foi demandado pela empresa fictícia de revenda de salgadinhos Duff Snacks.

O objetivo do cardápio é agilizar as vendas informatizando a empresa com um sistema fácil e intuitivo.

![bg left:33%](https://images.pexels.com/photos/6266283/pexels-photo-6266283.jpeg)

---

## Tecnologias Utilizadas

![bg right:33%](https://images.pexels.com/photos/1586951/pexels-photo-1586951.jpeg)

O sistema todo foi desenvolvido com .NET framework, mas também foram utilizadas algumas ferramentas adicionais:

|         Tecnologia | Descrição / Uso                                   |
| -----------------: | :------------------------------------------------ |
|      Windows Forms | Desenvolvimento da aplicação desktop              |
|          RestSharp | Comunicação entre a aplicação e a API             |
|     ASP.NET WebApi | Desenvolvimento da Rest API em arquitetura MVC    |
|   Entity Framework | ORM                                               |
|   FluentValidation | Regras de validação                               |
| SQL Server Express | Banco de dados relacional da Microsoft            |
|       Git & Gitlab | Controle de versionamento de código.              |
|       dbdiagram.io | Diagramas de Entidade de Relacionamento e Classes |
|              Figma | Diagram de casos de uso                           |

---

<!-- _class: center-all -->

## Telas

---

<!-- _class: center-all -->

## Diagramas

![bg](https://images.pexels.com/photos/1181311/pexels-photo-1181311.jpeg)

---

### Diagrama de Entidade de Relacionamento

![bg right:53.8%](./images/diagram-er.png)

---

<!-- _class: right -->

### Diagrama de Casos de Uso

![bg left:60%](./images/diagram-uc.png)

---

### Diagrama de Classes

![bg right:53.7%](./images/diagram-cl.png)

---

<!-- _class: center-all -->

## API

---

<!-- _class: center-all -->

## CÓDIGOS

---

<!-- _class: center-all -->

## Custos

| Resumo                     | Horas  | Valor/Hora |           Custo |
| :------------------------- | :----: | :--------: | --------------: |
| Desenvolvimento do Sistema |   88   |     60     |     R$ 5.280,00 |
| Implantação do Sistema     |   4    |     60     |       R$ 240,00 |
| **Total**                  | **92** |            | **R$ 5.520,00** |

---

<!-- _footer: 'Filype Rewel<br/>Diego Mascarenhas' -->
<!-- _class: center-h2 -->
<!-- _backgroundColor: #000 -->
<!-- _color: #fff -->

## FIM
